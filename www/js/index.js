
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        console.log('Received Device Ready Event');
        console.log('calling setup push');
        app.setupPush();
    },
    setupPush: function() {
        console.log('calling push init');
        var push = PushNotification.init({
            "android": {
                "senderID": "XXXXXXXX"
            },
            "browser": {},
            "ios": {
                "sound": true,
                "vibration": true,
                "badge": true
            },
            "windows": {}
        });
        console.log('after init');

        push.on('registration', function(data) {
            console.log('registration event: ' + data.registrationId);

            var oldRegId = localStorage.getItem('registrationId');
            if (oldRegId !== data.registrationId) {
                // Save new registration ID
                localStorage.setItem('registrationId', data.registrationId);
                // Post registrationId to your app server as the value has changed
            }

            var parentElement = document.getElementById('registration');
            var listeningElement = parentElement.querySelector('.waiting');
            var receivedElement = parentElement.querySelector('.received');

            listeningElement.setAttribute('style', 'display:none;');
            receivedElement.setAttribute('style', 'display:block;');
        });

        push.on('error', function(e) {
            console.log("push error = " + e.message);
        });

        push.on('notification', function(data) {
            console.log('notification event');
            navigator.notification.alert(
                data.message,         // message
                null,                 // callback
                data.title,           // title
                'Ok'                  // buttonName
            );
       });
    }
};
function registerPushwooshAndroid() {
    var pushNotification = cordova.require("com.misiva.plugins.pushwoosh.beacons.PushNotification");
    document.addEventListener("push-notification", function(b) {
        var notification = b.notification;
        alert(notification.title);
    });
    pushNotification.onDeviceReady({
        projectid: "YOUR SENDER ID"
    });
    pushNotification.registerDevice(function(b) {  onPushwooshAndroidInitialized(b) }, function(b) {
        console.warn(JSON.stringify(["failed to register ", b]))
    })
}
function onPushwooshAndroidInitialized(b) {
    alert("push token: " + b);
    var a = cordova.require("com.misiva.plugins.pushwoosh.beacons.PushNotification");
    a.getPushToken(function(c) {
        console.warn("push token: " + c)
    });
    a.getPushwooshHWID(function(c) {
        console.warn("Pushwoosh HWID: " + c)
    });
    a.getTags(function(c) {
        console.warn("tags for the device: " + JSON.stringify(c))
    }, function(c) {
        console.warn("get tags error: " + JSON.stringify(c))
    });
    a.setLightScreenOnNotification(false);
    a.setTags({ deviceName: "hello",        
                deviceId: 10    }, function(c) {console.warn("setTags success")}, function(c) {
        console.warn("setTags failed")
    })
}
